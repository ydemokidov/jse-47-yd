package com.t1.yd.tm.command.user;

import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.request.user.UserGetProfileRequest;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;

public class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_show_profile";

    @NotNull
    private final String description = "Show user profile";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE:]");

        @NotNull final UserGetProfileRequest request = new UserGetProfileRequest();
        request.setToken(getToken());

        @NotNull final UserDTO userDTO = getAuthEndpoint().getProfile(request).getUserDTO();

        showUser(userDTO);
        System.out.println("Last Name: " + userDTO.getLastName());
        System.out.println("First Name: " + userDTO.getFirstName());
        System.out.println("Middle Name: " + userDTO.getMiddleName());
        System.out.println("Role: " + userDTO.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
