package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.model.UserDTO;
import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    private UserDTO userDTO;

    public AbstractUserResponse(@Nullable final UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public AbstractUserResponse(@Nullable Throwable throwable) {
        super(throwable);
    }

    public AbstractUserResponse() {
    }

}
