package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.dto.model.UserDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserPasswordChangeResponse extends AbstractUserResponse {

    public UserPasswordChangeResponse(@Nullable final UserDTO userDTO) {
        super(userDTO);
    }

    public UserPasswordChangeResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
