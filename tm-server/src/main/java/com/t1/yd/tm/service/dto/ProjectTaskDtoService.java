package com.t1.yd.tm.service.dto;

import com.t1.yd.tm.api.repository.dto.IDtoProjectRepository;
import com.t1.yd.tm.api.repository.dto.IDtoTaskRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.dto.IDtoProjectTaskService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.ProjectIdEmptyException;
import com.t1.yd.tm.exception.field.TaskIdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.repository.dto.ProjectDtoRepository;
import com.t1.yd.tm.repository.dto.TaskDtoRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;

@AllArgsConstructor
public final class ProjectTaskDtoService implements IDtoProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;


    @NotNull
    private IDtoProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    @NotNull
    private IDtoTaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();

        try {
            @NotNull final IDtoProjectRepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final IDtoTaskRepository taskRepository = getTaskRepository(entityManager);
            @Nullable final TaskDTO taskDTO = taskRepository.findOneById(userId, taskId);
            if (taskDTO == null) throw new TaskNotFoundException();

            taskDTO.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(taskDTO);

            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IDtoProjectRepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final IDtoTaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final List<TaskDTO> taskDTOS = taskRepository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO taskDTO : taskDTOS) {
                taskRepository.removeById(taskDTO.getId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IDtoProjectRepository projectRepository = getProjectRepository(entityManager);
            if (projectRepository.findOneById(userId, projectId) == null)
                throw new ProjectNotFoundException();

            @NotNull final IDtoTaskRepository taskRepository = getTaskRepository(entityManager);
            @Nullable final TaskDTO taskDTO = taskRepository.findOneById(userId, taskId);
            if (taskDTO == null) throw new TaskNotFoundException();

            taskDTO.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(taskDTO);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
